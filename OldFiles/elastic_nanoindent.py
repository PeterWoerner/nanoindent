# -*- coding: utf-8 -*-
"""
Spyder Editor

Elastic.py
Simulates a linear elastic material on a unit square as a template for future examples.
"""

from __future__ import print_function
from dolfin import *
import numpy


def initialize():
    global E
    global nu
    global mu
    global lmbda
    global rho
    global h0
    global S
    global Ss
    mesh = domain()
    params = {}
    params['mesh'] = mesh
    E = 1.0e9 #Elastic Modulus
    nu = 0.3 #Poisson Ratio
    mu = E/(2.0*(1.0 + nu))  #Lame constants
    lmbda = E*nu/((1.0 + nu)*(1.0 - 2.0*nu))
    rho = 3.98
    h0 = 1e9
    S = 1e6
    Ss = 1e9

    #Create Function Space
    V = VectorFunctionSpace(mesh, "Lagrange", 1)
    params['V'] = V
    return params
    
class Left(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[0], 0.0)

class Right(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[0], 1.0)

class Bottom(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[1], 0.0)

class Top(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[1], 1.0)
        
class SlipSystem():
    def _init(self, mesh, m, s, V):
        self.mesh = mesh
        self.m = m
        self.s = s
        self.V = V
        self.gamma = project(Constant(0.0),V)
        self.h = project(h0,V)
        self.S = project(S, V)
    def updateSlip(self, sigma, ):
        return 0
    
    
def elasticCalc(params):
    if not has_linear_algebra_backend("PETSc"):
        print("DOLFIN has not been configured with PETSc. Exiting.")
        exit()
    left = Left()
    top = Top()
    right = Right()
    bottom = Bottom()    
    
    parameters["linear_algebra_backend"] = "PETSc"
    mesh = params['mesh']
    V = params['V']
    #Define variational problem
    u = TrialFunction(V)
    v = TestFunction(V)
    a = inner(sigma(u),grad(v))*dx
    f = Expression(("std::abs(rho*0.0)", "rho*0.0"), rho = rho)
    #L = v*dx
    L = inner(v,f)*dx
    # Define boundary condition
    
    # Initialize mesh function for boundary domains
    boundaries = FacetFunction("size_t", mesh)
    boundaries.set_all(0)
    left.mark(boundaries, 1)
    top.mark(boundaries, 2)
    right.mark(boundaries, 3)
    bottom.mark(boundaries, 4)
    
    u0 = Constant((0.0,0.0))
    #u1 = Constant((0.0,0.0))
    u1 = Expression((("0.0","std::min(sin(pi/3)*std::abs(x[0]-0.5)-ymax,0.0)")), ymax=.1)
    # Define Boundary Conditions
    bc4 = DirichletBC(V, u0, "x[1] <  DOLFIN_EPS"  ) #Bottom
    bc1 = DirichletBC(V, u0, "x[0] < DOLFIN_EPS") #Left
    bc2 = DirichletBC(V, u1, "x[1] > 1- DOLFIN_EPS") #Top
    bc3 = DirichletBC(V, u0, "x[0] > 1 - DOLFIN_EPS") #Right
    bcs = [bc1, bc2, bc3, bc4]#, bc2]
    #Define solution function
    u = Function(V)
    
    solve(a==L, u, bcs)

    #Save to file
#    file = File("test.pvd")
#    file << u
#    
    #Get Tractions  
    
    W = TensorFunctionSpace(mesh, "Lagrange", 2)
    sigmaw = project(sigma(u), W)
    I = Identity(u.geometric_dimension())
    epsilon = project(grad(u)+transpose(grad(u))/2.0,W)
    n = FacetNormal(mesh)
    #sigmaw[1,1] = (sigmaw[1,1] + abs(sigmaw[1,1]))/2
    
    sigmapw = project(sigmap(u),W)
    T = dot(sigmaw, n)

    
#    T = Constant((0.0,0.1))
    # New solve from traction
    L = inner(v,f)*dx - inner(T,v)*ds
    bcs = [bc1, bc3, bc4]
    uc = Function(V) # u corrector
    
    solve(a==L, uc, bcs)   

     #Plot Solution
#    plot(u1[1], mesh)
    #plot(mesh)
#    plot(epsilon[0,0], title = "epsilonxx")
    plot(sigmaw[0,0], title = "sigmaxx")
    plot(sigmaw[0,1], title = "sigmaxy")
    plot(sigmaw[1,1], title = "sigmayy")
    plot(sigmapw[0,0], title = "sigmapxx")
    plot(sigmapw[0,1], title = "sigmapxy")
    plot(sigmapw[1,1], title = "sigmapyy")
    
    sigmac = project(sigma(uc), W)
    plot(sigmaw[0,0], title = "sigmacxx")
    plot(sigmaw[0,1], title = "sigmacxy")
    plot(sigmaw[1,1], title = "sigmacyy")
#    plot(div(u), title= "Div(u)")

    plot(uc, mode = "displacement", wireframe=True, title = "Displacement")
    plot(u, mode = "displacement", wireframe=True, interactive=True, title = "Displacement")
    
def sigma(v):
    gdim = v.geometric_dimension()
    return 2.0*mu*sym(grad(v))+ lmbda*tr(sym(grad(v)))*Identity(gdim)
    
def sigmap(v):
    gdim = v.geometric_dimension()
    return (sigma(v) + abs(sigma(v)))/2

def domain():
    mesh = UnitSquareMesh(16,16)
    return mesh



def main():
    params = initialize()
    elasticCalc(params)
    


if __name__ == '__main__':
  main()


