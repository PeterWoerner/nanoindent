# -*- coding: utf-8 -*-
"""
Spyder Editor

Elastic.py
Simulates a linear elastic material on a unit square as a template for future examples.
"""

from __future__ import print_function
from dolfin import *
import numpy
import matplotlib.pyplot as plt


def initialize():
    global E
    global nu
    global mu
    global lmbda
    global rho
    global h0
    global S
    global Ss
    mesh = domain()
    params = {}
    params['mesh'] = mesh
    E = 1.0e9 #Elastic Modulus
    nu = 0.3 #Poisson Ratio
    mu = E/(2.0*(1.0 + nu))  #Lame constants
    lmbda = E*nu/((1.0 + nu)*(1.0 - 2.0*nu))
    rho = 3.98
    h0 = 1e9
    S = 1e6
    Ss = 1e9

    #Create Function Space
    V = VectorFunctionSpace(mesh, "Lagrange", 3)

    params['V'] = V
    return params
    
class Left(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[0], 0.0)

class Right(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[0], 1.0)

class Bottom(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[1], 0.0)

class Top(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[1], 1.0)
        
class SlipSystem():
    def _init(self, mesh, m, s, V):
        self.mesh = mesh
        self.m = m
        self.s = s
        self.V = V
        self.gamma = project(Constant(0.0),V)
        self.h = project(h0,V)
        self.S = project(S, V)
    def updateSlip(self, sigma, ):
        return 0
 



    
def elasticCalc(params):
    if not has_linear_algebra_backend("PETSc"):
        print("DOLFIN has not been configured with PETSc. Exiting.")
        exit()
    RightM = AutoSubDomain(lambda x, on_bnd: near(x[0], 1) and on_bnd)
    TopM   = AutoSubDomain(lambda x, on_bnd: near(x[1], 1) and on_bnd)
    left = Left()
    top = Top()
    right = Right()
    bottom = Bottom()    
    snes_solver_parameters = {"nonlinear_solver": "snes",
                          "snes_solver"     : { "linear_solver"   : "lu",
                                                "maximum_iterations": 20,
                                                "report": True,
                                                "error_on_nonconvergence": False,
                                               }}
    parameters["linear_algebra_backend"] = "PETSc"
    mesh = params['mesh']
    V = params['V']
    W = FunctionSpace(mesh, "Lagrange", 1)
    gamma = Function(W)
    gamma = interpolate(Constant(0.0), W)
    #Define variational problem
    du = TrialFunction(V)  # Incremental displacement (Variation on U)
    v = TestFunction(V)   # Test Function
    u = Function(V)      # Displacement from previous iteration
    m = Constant((1.0/sqrt(2), 1.0/sqrt(2)))
    s = Constant((-1.0/sqrt(2), 1.0/sqrt(2)))
    B = m*gamma #Body force per unit volume
    T = Constant((0.0, 0.0)) #Traction
    
    # Kinematics
    d = u.geometric_dimension()
    I = Identity(d)
    F = I + grad(u)
    C = F.T*F
    epsilon = sym(grad(u))
    cp = numpy.zeros((2,2,2,2)) #compliance tensor
    cp[0,0,0,0]= 1
#   Invariants of deformation tensors
    Ic = tr(C)
    J = det(F)
     
    ## Nanoindenter location
    ymax = 0.1
    u1 = Expression((("0.0","std::min(sin(pi/3)*std::abs(x[0]-0.5)-2*ymax,0.0)")), ymax=ymax)
    #Define Total Strain Energy (assuming small strain) and linear isotropic
    psi = inner(lmbda*tr(epsilon)*I + 2* mu* epsilon, epsilon)
    Pi = psi*dx - dot(B, u)*dx + 1e12*(numpy.abs(u1[1]-u[1]) - (u1[1]-u[1]))*u[1]*ds

    #Directional derivative
    F = derivative(Pi, u, v)
     
    #Jacobian of F
    J = derivative(F, u, du)
    
    # Define Boundary conditions
    u0 = Constant((0.0,0.0))
    #u1 = Expression((("0.0","std::min(sin(pi/3)*std::abs(x[0]-0.5)-ymax,0.0)")), ymax=.1)
    bc4 = DirichletBC(V, u0, bottom) #Bottom
    bc1 = DirichletBC(V, u0, left) #Left
    #bc2 = DirichletBC(V, u1, top) #Top
    bc3 = DirichletBC(V, u0, right) #Right
    bcs = [bc1, bc3, bc4]#, bc2]    
    #Variational Problem
    problem = NonlinearVariationalProblem(F, u, bcs, J=J)
    solver = NonlinearVariationalSolver(problem)
    solver.parameters.update(snes_solver_parameters)
    info(solver.parameters, True)
    
    
    solver.solve()
    sig = sigma(u)
    gamma = gamma + dot(dot(s, sig), m)
    B = m*dot(grad(gamma), s) #Body force per unit volume
    
    plot(u, mode = "displacement")
    
    
    ymax = 0.0
    u1 = Expression((("0.0","std::min(sin(pi/3)*std::abs(x[0]-0.5)-2*ymax,0.0)")), ymax=ymax)
    #Define Total Strain Energy (assuming small strain) and linear isotropic
    psi = inner(lmbda*tr(epsilon)*I + 2* mu* epsilon, epsilon)
    Pi = psi*dx - dot(B, u)*dx# + 1e12*(numpy.abs(u1[1]-u[1]) - (u1[1]-u[1]))*u[1]*ds

    #Directional derivative
    F = derivative(Pi, u, v)
     
    #Jacobian of F
    J = derivative(F, u, du)
    problem = NonlinearVariationalProblem(F, u, bcs, J=J)
    solver = NonlinearVariationalSolver(problem)
    solver.solve()
    #solver.parameters.update(snes_solver_parameters)
    #info(solver.parameters, True)
    plot(u)
    print(u(0.5,1.0))
    print(ymax)
    #plot(project(u1,V))
    return


def sigma(v):
    gdim = v.geometric_dimension()
    return 2.0*mu*sym(grad(v))+ lmbda*tr(sym(grad(v)))*Identity(gdim)
    
def sigmap(v):
    gdim = v.geometric_dimension()
    return (sigma(v) + abs(sigma(v)))/2

def domain():
    mesh = UnitSquareMesh(16,16)
    return mesh


    

def main():
    params = initialize()
    elasticCalc(params)
    


if __name__ == '__main__':
  main()


