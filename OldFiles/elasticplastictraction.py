## Author: Peter Woerner
#  

from __future__ import print_function
from dolfin import *
import numpy as np
import matplotlib.pyplot as plt
import scipy
import scipy.optimize
import scipy.interpolate
import timeit

def initialize():
    global E
    global nu
    global mu
    global lmbda
    global rho
    global h0
    global S
    global Ss
    mesh = domain()
    params = {}
    params['mesh'] = mesh
    E = 1.0e9 #Elastic Modulus
    nu = 0.3 #Poisson Ratio
    mu = E/(2.0*(1.0 + nu))  #Lame constants
    lmbda = E*nu/((1.0 + nu)*(1.0 - 2.0*nu))
    rho = 3.98
    h0 = 1e9
    S = 1e6
    Ss = 1e9
    params['mu'] = mu
    params['lmbda'] = lmbda
 
    
    #Create Function Space
    V = VectorFunctionSpace(mesh, "Lagrange", 1)
    params['V'] = V
    m = Constant((1/sqrt(2), -1/sqrt(2)))
    s = Constant((1/sqrt(2), 1/sqrt(2)))
    a = 1
    St = 1e6
    d = 1
    slip1= SlipSystem(mesh, m, s, V, h0, St, a, d)
    params['slip1'] = slip1
    return params
    
class Left(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[0], 0.0)

class Right(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[0], 1.0)

class Bottom(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[1], 0.0)

class Top(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[1], 1.0)

class tractionInterpolate(Expression):
    def eval(self, value, x):
        spl = scipy.interpolate.InterpolatedUnivariateSpline(self.x, self.y)
        value[0] = spl(x[0])
    def _init(self):
        self.x = np.linspace(0,1,8)
        self.y = 0*x
    def assignPoints(self, x,y):
        self.x = x
        self.y = y
    def value_shape(self):
        return(3,)
    
class SlipSystem(Expression):
    def __init__(self, mesh, m, s, V, h0, St, a, d):
        self.mesh = mesh
        self.m = m
        self.s = s
        self.V = V
        self.V0 = FunctionSpace(self.mesh, "Lagrange", 1)
        self.gamma = project(0,self.V0)
        self.h = project(h0,self.V0)
        self.St = project(St, self.V0)
        self.Smax = Smax
        self.a = a
        self.d = d
    def updateSlip(self, sigma, dt):
        tau = dot(dot(s,sigma),m)
        self.h = (1 - self.St/self.Smax)^self.a
        self.gamma = self.gamma + dt*self.d*(abs(tau)/St)^(1/self.m)*tau/abs(tau)
        return 0
    def eval(self, value, x):
        value = dot(grad(self.gamma),self.s)*self.m - dot(grad(self.gamma), self.m)*self.s
        
def elasticCalc(params):
    ## Calculate with traction on top
    if not has_linear_algebra_backend("PETSc"):
        print("DOLFIN has not been configured with PETSc. Exiting.")
        exit()
    RightM = AutoSubDomain(lambda x, on_bnd: near(x[0], 1) and on_bnd)
    TopM   = AutoSubDomain(lambda x, on_bnd: near(x[1], 1) and on_bnd)
    left = Left()
    top = Top()
    right = Right()
    bottom = Bottom()    
    snes_solver_parameters = {"nonlinear_solver": "snes",
                          "snes_solver"     : { "linear_solver"   : "lu",
                                                "maximum_iterations": 20,
                                                "report": True,
                                                "error_on_nonconvergence": False,
                                               }}
    parameters["linear_algebra_backend"] = "PETSc"
    mesh = params['mesh']
    V = params['V']
    #Define variational problem
    du = TrialFunction(V)  # Incremental displacement (Variation on U)
    v = TestFunction(V)   # Test Function
    u = Function(V)      # Displacement from previous iteration
    B = Constant((0.0, 0.0)) #Body force per unit volume
    T = params['T']

    
    # Kinematics
    d = u.geometric_dimension()
    I = Identity(d)
    F = I + grad(u)
    C = F.T*F
    epsilon = sym(grad(u))
#   Invariants of deformation tensors
    Ic = tr(C)
    J = det(F)
     
    ## Nanoindenter location

    psi = inner(lmbda*tr(epsilon)*I + 2* mu* epsilon, epsilon)
    Pi = psi*dx - dot(B, u)*dx - T[0]*u[1]*ds# + dot(penalty(u, project(u1, V)),u)*ds
    
    #Directional derivative
    F = derivative(Pi, u, v)
     
    #Jacobian of F
    J = derivative(F, u, du)
    
    # Define Boundary conditions
    u0 = Constant((0.0,0.0))
    #u1 = Expression((("0.0","std::min(sin(pi/3)*std::abs(x[0]-0.5)-ymax,0.0)")), ymax=.1)
    bc4 = DirichletBC(V, u0, bottom) #Bottom
    bc1 = DirichletBC(V, u0, left) #Left
    #bc2 = DirichletBC(V, u1, top) #Top
    bc3 = DirichletBC(V, u0, right) #Right
    bcs = [bc1, bc3, bc4]#, bc2] 
    #Variational Problem
    problem = NonlinearVariationalProblem(F, u, bcs, J=J)
    solver = NonlinearVariationalSolver(problem)
    solver.parameters.update(snes_solver_parameters)
    info(solver.parameters, True)


    solver.solve()

    #plot(u, mode = "displacement", title = "displacement")
    

    return u
    
def elasticCalcD(params):
    ## Calculate with only dirichlet boundary conditions
    if not has_linear_algebra_backend("PETSc"):
        print("DOLFIN has not been configured with PETSc. Exiting.")
        exit()
    RightM = AutoSubDomain(lambda x, on_bnd: near(x[0], 1) and on_bnd)
    TopM   = AutoSubDomain(lambda x, on_bnd: near(x[1], 1) and on_bnd)
    left = Left()
    top = Top()
    right = Right()
    bottom = Bottom()    
    snes_solver_parameters = {"nonlinear_solver": "snes",
                          "snes_solver"     : { "linear_solver"   : "lu",
                                                "maximum_iterations": 20,
                                                "report": True,
                                                "error_on_nonconvergence": False,
                                               }}
    parameters["linear_algebra_backend"] = "PETSc"
    mesh = params['mesh']
    V = params['V']
    #Define variational problem
    du = TrialFunction(V)  # Incremental displacement (Variation on U)
    v = TestFunction(V)   # Test Function
    u = Function(V)      # Displacement from previous iteration
    slip1 = params('slip1')
    B = Constant((0.0, 0.0)) #Body force per unit volume
    u1 = params['u1']
    # Kinematics
    d = u.geometric_dimension()
    I = Identity(d)
    F = I + grad(u)
    C = F.T*F
    epsilon = sym(grad(u))
    cp = np.zeros((2,2,2,2)) #compliance tensor
    cp[0,0,0,0]= 1
#   Invariants of deformation tensors
    Ic = tr(C)
    J = det(F)
     
    ## Nanoindenter location

    psi = inner(lmbda*tr(epsilon)*I + 2* mu* epsilon, epsilon)
    Pi = psi*dx - dot(B, u)*dx- dot(slip1,u)# + dot(penalty(u, project(u1, V)),u)*ds
    
    #Directional derivative
    F = derivative(Pi, u, v)
     
    #Jacobian of F
    J = derivative(F, u, du)
    
    # Define Boundary conditions
    u0 = Constant((0.0,0.0))
    #u1 = Expression((("0.0","std::min(sin(pi/3)*std::abs(x[0]-0.5)-ymax,0.0)")), ymax=.1)
    bc4 = DirichletBC(V, u0, bottom) #Bottom
    bc1 = DirichletBC(V, u0, left) #Left
    bc2 = DirichletBC(V, u1, top) #Top
    bc3 = DirichletBC(V, u0, right) #Right
    bcs = [bc1, bc3, bc4, bc2] 
    #Variational Problem
    problem = NonlinearVariationalProblem(F, u, bcs, J=J)
    solver = NonlinearVariationalSolver(problem)
    solver.parameters.update(snes_solver_parameters)
    info(solver.parameters, True)


    solver.solve()

    return u

def sigma(v, params):
    mu = params['mu']
    lmbda = params['lmbda']
    gdim = v.geometric_dimension()
    return 2.0*mu*sym(grad(v))+ lmbda*tr(sym(grad(v)))*Identity(gdim)
    
def sigmap(v):
    gdim = v.geometric_dimension()
    return (sigma(v) + abs(sigma(v)))/2

def domain():
    mesh = UnitSquareMesh(32,32)
    return mesh


def runFEMcode2(y, x, u1):
    params = initialize()
    T = tractionInterpolate()
    T.assignPoints(x,y)
    params['T'] = T    
    params['u1'] = u1
    u = elasticCalc(params)
    error = energyPenalty(u, u1, params)
    return(error)
    
    
def energyPenalty(u,u1,params):
    mesh = params['mesh']
    V = params['V']
    epsilon = sym(grad(u))
    stress = sigma(u,params)
    u2 = project(u1, V)
    penalty = (u-u2)+abs(u-u2)
    k = params['mu']*1000000
    C = Constant(('0.0', '1.0'))

    energy = assemble(inner(epsilon, stress)*ds+inner(k*penalty,C)*ds)
    print(energy)
    return energy

def initialGuessFEM(u1,x):
    params = initialize()
    params['u1'] = u1
    print(u1)
    u = elasticCalcD(params)
    stess = sigma(u, params)
    mesh = params['mesh']
    n = FacetNormal(mesh)
    W = TensorFunctionSpace(mesh, "Lagrange", 2)
    stress = project(stess, W)
    T = dot(stress, n)
    y1 = x*0
    y0 = x*0
    i = 0
    for point in x:
        data = stress(point, 1)
        y1[i] = data[3]
        y0[i] = data[2]
        i = i+1

    y1 = (y1 - abs(y1))/2
    plot(u, mode = "displacement", title = "Initial Displacement")
#    VTKPlotter.write_pdf(plot(u, mode = "displacement", title = "Dirichlet Displacement"))    
    return y1  
    
    
def runFEMcode2Plot(y, x, u1):
    params = initialize()
    T = tractionInterpolate()
    T.assignPoints(x,y)
    params['T'] = T    
    params['u1'] = u1
    u = elasticCalc(params)
    plot(u, mode = "displacement", title = "Optimized Displacement", legend = "Displacement (%)")
    VTKPlotter.write_pdf(plot(u, mode = "displacement", title = "Optimized Displacement", legend = "Displacement (%)"))
    #error = boundaryError(u, u1, T)
    #error = energyPenalty(u,u1,params)
    #error = boundaryError(u, u1, T)
    return(u)
    
    

def singleSimulation():
    start = timeit.default_timer()

    x = np.linspace(0,1,32)
    u1 = Expression((("0.0","std::min(sin(pi/3)*std::abs(x[0]-0.5)-ymax,0.00)")), ymax=0.05)
    y = initialGuessFEM(u1,x)
    u1 = Expression((("0.0","sin(pi/3)*std::abs(x[0]-0.5)-ymax")), ymax=0.05)
    result = scipy.optimize.minimize(runFEMcode2, y, args = (x, u1), method = 'Nelder-Mead', options={'maxiter':500})
    
    #result2 = scipy.optimize.minimize(runFEMcode2, result.x, args = (x, u1), method = 'Nelder-Mead')
    #runFEMcode2Plot(result.x, x, u1)
    runFEMcode2Plot(y,x,u1)
    print(y)
    print(result.x)
    #print(result)
    stop = timeit.default_timer()
    print(stop - start) 

def main():
    singleSimulation()


if __name__ == '__main__':
  main()

