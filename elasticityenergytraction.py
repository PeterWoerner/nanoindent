# -*- coding: utf-8 -*-
"""
Spyder Editor

Elastic.py
Simulates a linear elastic material on a unit square as a template for future examples.
"""

from __future__ import print_function
from dolfin import *
import numpy as np
import matplotlib.pyplot as plt
import scipy
import scipy.optimize
import scipy.interpolate
import timeit

def initialize():
    global E
    global nu
    global mu
    global lmbda
    global rho
    global h0
    global S
    global Ss
    mesh = domain()
    params = {}
    params['mesh'] = mesh
    E = 1.0e9 #Elastic Modulus
    nu = 0.3 #Poisson Ratio
    mu = E/(2.0*(1.0 + nu))  #Lame constants
    lmbda = E*nu/((1.0 + nu)*(1.0 - 2.0*nu))
    rho = 3.98
    h0 = 1e9
    S = 1e6
    Ss = 1e9
    params['mu'] = mu
    params['lmbda'] = lmbda
    #Create Function Space
    V = VectorFunctionSpace(mesh, "Lagrange", 1)
    params['V'] = V
    return params
    
class Left(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[0], 0.0)

class Right(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[0], 1.0)

class Bottom(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[1], 0.0)

class Top(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[1], 1.0)
        
class SlipSystem():
    def _init(self, mesh, m, s, V):
        self.mesh = mesh
        self.m = m
        self.s = s
        self.V = V
        self.gamma = project(Constant(0.0),V)
        self.h = project(h0,V)
        self.S = project(S, V)
    def updateSlip(self, sigma, ):
        return 0

class MyExpression(Expression):
  def eval(self, value, x):
    if x[0] <= 0:
      value[0] = 1.0
    else:
      value[0] = 0.0

  def value_shape(self):
    return (1,)

class tractionInterpolate(Expression):
    def eval(self, value, x):
        spl = scipy.interpolate.InterpolatedUnivariateSpline(self.x, self.y)
        value[0] = spl(x[0])
    def _init(self):
        self.x = np.linspace(0,1,8)
        self.y = 0*x
    def assignPoints(self, x,y):
        self.x = x
        self.y = y
    def value_shape(self):
        return(2,)
    
    
def elasticCalc(params):
    if not has_linear_algebra_backend("PETSc"):
        print("DOLFIN has not been configured with PETSc. Exiting.")
        exit()
    RightM = AutoSubDomain(lambda x, on_bnd: near(x[0], 1) and on_bnd)
    TopM   = AutoSubDomain(lambda x, on_bnd: near(x[1], 1) and on_bnd)
    left = Left()
    top = Top()
    right = Right()
    bottom = Bottom()    
    snes_solver_parameters = {"nonlinear_solver": "snes",
                          "snes_solver"     : { "linear_solver"   : "lu",
                                                "maximum_iterations": 20,
                                                "report": True,
                                                "error_on_nonconvergence": False,
                                               }}
    parameters["linear_algebra_backend"] = "PETSc"
    mesh = params['mesh']
    V = params['V']
    #Define variational problem
    du = TrialFunction(V)  # Incremental displacement (Variation on U)
    v = TestFunction(V)   # Test Function
    u = Function(V)      # Displacement from previous iteration
    B = Constant((0.0, 0.0)) #Body force per unit volume
    T = params['T']

    
    # Kinematics
    d = u.geometric_dimension()
    I = Identity(d)
    F = I + grad(u)
    C = F.T*F
    epsilon = sym(grad(u))
    cp = np.zeros((2,2,2,2)) #compliance tensor
    cp[0,0,0,0]= 1
#   Invariants of deformation tensors
    Ic = tr(C)
    J = det(F)
     
    ## Nanoindenter location

    psi = inner(lmbda*tr(epsilon)*I + 2* mu* epsilon, epsilon)
    Pi = psi*dx - dot(B, u)*dx - T[0]*u[1]*ds# + dot(penalty(u, project(u1, V)),u)*ds
    
    #Directional derivative
    F = derivative(Pi, u, v)
     
    #Jacobian of F
    J = derivative(F, u, du)
    
    # Define Boundary conditions
    u0 = Constant((0.0,0.0))
    #u1 = Expression((("0.0","std::min(sin(pi/3)*std::abs(x[0]-0.5)-ymax,0.0)")), ymax=.1)
    bc4 = DirichletBC(V, u0, bottom) #Bottom
    bc1 = DirichletBC(V, u0, left) #Left
    #bc2 = DirichletBC(V, u1, top) #Top
    bc3 = DirichletBC(V, u0, right) #Right
    bcs = [bc1, bc3, bc4]#, bc2] 
    #Variational Problem
    problem = NonlinearVariationalProblem(F, u, bcs, J=J)
    solver = NonlinearVariationalSolver(problem)
    solver.parameters.update(snes_solver_parameters)
    info(solver.parameters, True)


    solver.solve()

    #plot(u, mode = "displacement", title = "displacement")
    

    return u

def elasticCalcD(params):
    if not has_linear_algebra_backend("PETSc"):
        print("DOLFIN has not been configured with PETSc. Exiting.")
        exit()
    RightM = AutoSubDomain(lambda x, on_bnd: near(x[0], 1) and on_bnd)
    TopM   = AutoSubDomain(lambda x, on_bnd: near(x[1], 1) and on_bnd)
    left = Left()
    top = Top()
    right = Right()
    bottom = Bottom()    
    snes_solver_parameters = {"nonlinear_solver": "snes",
                          "snes_solver"     : { "linear_solver"   : "lu",
                                                "maximum_iterations": 20,
                                                "report": True,
                                                "error_on_nonconvergence": False,
                                               }}
    parameters["linear_algebra_backend"] = "PETSc"
    mesh = params['mesh']
    V = params['V']
    #Define variational problem
    du = TrialFunction(V)  # Incremental displacement (Variation on U)
    v = TestFunction(V)   # Test Function
    u = Function(V)      # Displacement from previous iteration
    B = Constant((0.0, 0.0)) #Body force per unit volume
    u1 = params['u1']
    # Kinematics
    d = u.geometric_dimension()
    I = Identity(d)
    F = I + grad(u)
    C = F.T*F
    epsilon = sym(grad(u))
    cp = np.zeros((2,2,2,2)) #compliance tensor
    cp[0,0,0,0]= 1
#   Invariants of deformation tensors
    Ic = tr(C)
    J = det(F)
     
    ## Nanoindenter location

    psi = inner(lmbda*tr(epsilon)*I + 2* mu* epsilon, epsilon)
    Pi = psi*dx - dot(B, u)*dx# + dot(penalty(u, project(u1, V)),u)*ds
    
    #Directional derivative
    F = derivative(Pi, u, v)
     
    #Jacobian of F
    J = derivative(F, u, du)
    
    # Define Boundary conditions
    u0 = Constant((0.0,0.0))
    #u1 = Expression((("0.0","std::min(sin(pi/3)*std::abs(x[0]-0.5)-ymax,0.0)")), ymax=.1)
    bc4 = DirichletBC(V, u0, bottom) #Bottom
    bc1 = DirichletBC(V, u0, left) #Left
    bc2 = DirichletBC(V, u1, top) #Top
    bc3 = DirichletBC(V, u0, right) #Right
    bcs = [bc1, bc3, bc4, bc2] 
    #Variational Problem
    problem = NonlinearVariationalProblem(F, u, bcs, J=J)
    solver = NonlinearVariationalSolver(problem)
    solver.parameters.update(snes_solver_parameters)
    info(solver.parameters, True)


    solver.solve()

    #plot(u, mode = "displacement", title = "displacement")
    

    return u

def sigma(v, params):
    mu = params['mu']
    lmbda = params['lmbda']
    gdim = v.geometric_dimension()
    return 2.0*mu*sym(grad(v))+ lmbda*tr(sym(grad(v)))*Identity(gdim)
    
def sigmap(v):
    gdim = v.geometric_dimension()
    return (sigma(v) + abs(sigma(v)))/2

def domain():
    mesh = UnitSquareMesh(32,32)
    return mesh

def boundaryError(u,u1,t):
    #reads in the final solution u and the nanoindenter position, and traction and
# calculates an error
    
    error = 0
    x = np.linspace(0,1,100)
    for xpos in x:
        eval = u(xpos,1)
        eval = eval[1]
        npos = u1(xpos,1)
        npos = npos[1]
        if eval < npos:
            traction = t(xpos)
            error = error + abs(traction[0]/1e9)
        elif eval > npos:
            error = error + 100*(eval - npos)
        else:
            traction = t(xpos)
            if traction[0] < 0:
                error = error + 0
            else:
                error = error + traction[0]
              
    return error

def runFEMcode():
    params = initialize()
    ## Define Traction
    T = tractionInterpolate() #Traction
    x = np.linspace(0,1,16)
    y = -1e8*np.exp(-(x-0.5)*(x-0.5)/.01)
    T.assignPoints(x,y)
    params['T']  = T
    #define nanoindeter position
    u1 = Expression((("0.0","sin(pi/3)*std::abs(x[0]-0.5)-ymax")), ymax=.05)
    print(u1(1,1))
    params['u1'] = u1
    u = elasticCalc(params)

    #u1 = nanoindenter position
    error = boundaryError(u, u1, T)
    print(error)

def initialGuessFEM(u1,x):
    params = initialize()
    params['u1'] = u1
    print(u1)
    u = elasticCalcD(params)
    stess = sigma(u, params)
    mesh = params['mesh']
    n = FacetNormal(mesh)
    W = TensorFunctionSpace(mesh, "Lagrange", 2)
    stress = project(stess, W)
    T = dot(stress, n)
    y1 = x*0
    y0 = x*0
    i = 0
    for point in x:
        data = stress(point, 1)
        y1[i] = data[3]
        y0[i] = data[2]
        i = i+1

    y1 = (y1 - abs(y1))/2
    plot(u, mode = "displacement", title = "Initial Displacement")
#    VTKPlotter.write_pdf(plot(u, mode = "displacement", title = "Dirichlet Displacement"))    
    return y1

def runFEMcode2(y, x, u1):
    params = initialize()
    T = tractionInterpolate()
    T.assignPoints(x,y)
    params['T'] = T    
    params['u1'] = u1
    u = elasticCalc(params)
    #error = boundaryError(u, u1, T)
    error = energyPenalty(u, u1, params)
    return(error)
    
    
def energyPenalty(u,u1,params):
    mesh = params['mesh']
    V = params['V']
    epsilon = sym(grad(u))
    stress = sigma(u,params)
    u2 = project(u1, V)
    penalty = (u-u2)+abs(u-u2)
    k = params['mu']*1000000
    C = Constant(('0.0', '1.0'))

    energy = assemble(inner(epsilon, stress)*ds+inner(k*penalty,C)*ds)
    print(energy)
    return energy

    
    
    
def runFEMcode2Plot(y, x, u1):
    params = initialize()
    T = tractionInterpolate()
    T.assignPoints(x,y)
    params['T'] = T    
    params['u1'] = u1
    u = elasticCalc(params)
    plot(u, mode = "displacement", title = "Optimized Displacement", legend = "Displacement (%)")
    VTKPlotter.write_pdf(plot(u, mode = "displacement", title = "Optimized Displacement", legend = "Displacement (%)"))
    stess = sigma(u, params)
    mesh = params['mesh']
    n = FacetNormal(mesh)
    W = TensorFunctionSpace(mesh, "Lagrange", 2)
    stress = project(stess, W)
    T = dot(stress, n)
    plot(stress[0,0], title = "Sigma_xx", legend = "Sigma_xx")
    VTKPlotter.write_pdf(plot(stress[0,0], title = "Sigma_xx")
    VTKPlotter.write_pdf(plot(stress[1,0], title = "Sigma_yx")
    VTKPlotter.write_pdf(plot(stress[1,1], title = "Sigma_yy")
    plot(stress[1,0], title = "Sigma_yx", legend = "Sigma_yx")
    plot(stress[1,1], title = "Sigma_yy", legend = "Sigma_yy")
    interactive()
    #error = boundaryError(u, u1, T)
    #error = energyPenalty(u,u1,params)
    error = boundaryError(u, u1, T)
    return(u)
    
def timestep(t, dt):
    slope = 0.2    
    if t<0.5:    
      ymax = slope*t
    else:
      ymax = slope*(1.0 - t)
      
    x = np.linspace(0,1,32)
    u1 = Expression((("0.0","std::min(sin(pi/3)*std::abs(x[0]-0.5)-ymax,0.00)")), ymax=ymax)
    y = initialGuessFEM(u1,x)
    u1 = Expression((("0.0","sin(pi/3)*std::abs(x[0]-0.5)-ymax")), ymax=ymax)
    result = scipy.optimize.minimize(runFEMcode2, y, args = (x, u1), method = 'Nelder-Mead', options={'maxiter':500})
    u = runFEMcode2Plot(y,x,u1)
    return u
    
    
def singleSimulation():
    start = timeit.default_timer()

    x = np.linspace(0,1,32)
    u1 = Expression((("0.0","std::min(sin(pi/3)*std::abs(x[0]-0.5)-ymax,0.00)")), ymax=0.05)
    y = initialGuessFEM(u1,x)
    u1 = Expression((("0.0","sin(pi/3)*std::abs(x[0]-0.5)-ymax")), ymax=0.05)
    result = scipy.optimize.minimize(runFEMcode2, y, args = (x, u1), method = 'Nelder-Mead', options={'maxiter':500})
    
    #result2 = scipy.optimize.minimize(runFEMcode2, result.x, args = (x, u1), method = 'Nelder-Mead')
    #runFEMcode2Plot(result.x, x, u1)
    runFEMcode2Plot(y,x,u1)
    print(y)
    print(result.x)
    #print(result)
    stop = timeit.default_timer()
    print(stop - start) 
    
def main():
    singleSimulation()
#    start = timeit.default_timer()
#    t = 0.5;
#    dt = .1
#    results = []
#    while t < 1:
#        u = timestep(t,dt)
#        t = t + dt
#        results.append([t, u])
#    stop = timeit.default_timer()
#    print(stop - start)    
    
if __name__ == '__main__':
  main()


