# -*- coding: utf-8 -*-
"""
Created on Mon Dec  5 18:11:52 2016
Slip system for sapphire + linear isotropic material.

@author: peter
"""


from __future__ import print_function
from dolfin import *
import numpy
import matplotlib.pyplot as plt
import scipy.optimize



def initialize(*arg):
    global mu
    global lmbda
    global h0
    mesh = domain()
    params = {}
    params['mesh'] = mesh
    E = 1.0e9 #Elastic Modulus
    nu = 0.3 #Poisson Ratio
    mu = E/(2.0*(1.0 + nu))  #Lame constants
    lmbda = E*nu/((1.0 + nu)*(1.0 - 2.0*nu))
    rho = 3.98
    h0 = 1e9
    S = 1e6
    Ss = 1e9
    params['Ss'] = Ss
    params['S'] = S
    params['h0'] = h0
    params['rho'] = rho
    #Create Function Space
    V = VectorFunctionSpace(mesh, "Lagrange", 1)
    params['V'] = V
    W = FunctionSpace(mesh, "Lagrange", 1)
    params['W']= W
    gamma = Function(W)
    gamma = interpolate(Constant(0.0), W)
    params['gamma'] = gamma
    params['TFS'] = TensorFunctionSpace(mesh, "Lagrange", 1)
    slipSystems = [];
    basalSlip1 = slipSystem(mesh, Constant((1.0,0.0)), Constant((0.0,1.0)), V, params['h0'],params['Ss'], 1, 1, .06, gamma,W)
    slipSystems.append(basalSlip1)
    params['slipSystems'] = slipSystems
    return params
    
class Left(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[0], 0.0)

class Right(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[0], 1.0)

class Bottom(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[1], 0.0)

class Top(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[1], 1.0)
                
def sigma(v, mu, lmbda):
    gdim = v.geometric_dimension()
    return 2.0*mu*sym(grad(v))+ lmbda*tr(sym(grad(v)))*Identity(gdim)
    
def sigmap(v):
    gdim = v.geometric_dimension()
    return (sigma(v) + abs(sigma(v)))/2

def domain():
    mesh = UnitSquareMesh(16,16)
    return mesh
    

    
class slipSystem():
    def __init__(self, mesh, m, s, V, h0, St,mm, a, d, gamma, W):
        self.mesh = mesh
        self.m = m
        self.s = s
        self.V = V
        self.h0 = h0
        self.St = St
        self.a = a
        self.mm = mm
        self.d = d
        self.gamma = gamma
        self.h = gamma*0
        self.S = gamma*0
        self.W = W

def slipSystemUpdate(slipSystems, dt, sig):
    for system in slipSystems:
        tau = project(dot(dot(system.m, sig),system.s))
        aa = project(tau)
        a = (tau**2)**(0.5)
        b = absu(project(tau)/system.St)
        system.gamma = system.gamma + signu(tau)*system.d*(b)**(1/system.mm)
        one = project(1,system.W)
        for system2 in slipSystems:
           h = system2.h*(1-system2.S/system2.St)**system.a
           if system.m == system2.m:
               q = 1
           else:
               q = 0.4   
           system.S = system.S +  q*h*((one-system2.S/system2.St + absu(one-system2.S/system2.St))/2)
        #print(system.gamma)
        #print(project(system.gamma))


def absu(v):
    return sqrt(pow(v,2))
    
def signu(v):
    return v/absu(v)
        
         
        

def slipBodyForce(slipSystems, dim, V):
    if dim==2: b = Constant((0.0,0.0))
    if dim == 3: b = Constant((0.0,0.0,0.0))
    B = interpolate(b, V)
    for system in slipSystems:
        B = B + inner(system.s,grad(system.gamma))*system.m
    return B
    

def elasticCalc(params):
    if not has_linear_algebra_backend("PETSc"):
        print("DOLFIN has not been configured with PETSc. Exiting.")
        exit()
    left = Left()
    top = Top()
    
    right = Right()
    bottom = Bottom()    
    snes_solver_parameters = {"nonlinear_solver": "snes",
                          "snes_solver"     : { "linear_solver"   : "lu",
                                                "maximum_iterations": 20,
                                                "report": True,
                                                "error_on_nonconvergence": False,
                                               }}
    parameters["linear_algebra_backend"] = "PETSc"
    mesh = params['mesh']
    V = params['V']

    W = params['W']
    gamma = params['gamma']
    #Define variational problem
    du = TrialFunction(V)  # Incremental displacement (Variation on U)
    v = TestFunction(V)   # Test Function
    u = Function(V)      # Displacement from previous iteration
    slipSystems = params['slipSystems']
    B = slipBodyForce(slipSystems, 2, V)
    # print('Hello')
    # Kinematics
    d = u.geometric_dimension()
    I = Identity(d)
    F = I + grad(u)
    C = F.T*F
    epsilon = sym(grad(u))

#   Invariants of deformation tensors
    Ic = tr(C)
    J = det(F)
     
    ## Nanoindenter location
    ymax = params['ymax']
    u1 = Expression((("0.0","std::min(sin(pi/3)*std::abs(x[0]-0.5)-2*y,0.0)")), y=ymax)
    #Define Total Strain Energy (assuming small strain) and linear isotropic
    psi = inner(lmbda*tr(epsilon)*I + 2* mu* epsilon, epsilon)
    Pi = psi*dx - dot(B, u)*dx + 1e12*(numpy.abs(u1[1]-u[1]) - (u1[1]-u[1]))*u[1]*ds

        #Directional derivative
    F = derivative(Pi, u, v)
     
    #Jacobian of F
    J = derivative(F, u, du)
    
    # Define Boundary conditions
    u0 = Constant((0.0,0.0))
    #u1 = Expression((("0.0","std::min(sin(pi/3)*std::abs(x[0]-0.5)-ymax,0.0)")), ymax=.1)
    bc4 = DirichletBC(V, u0, bottom) #Bottom
    bc1 = DirichletBC(V, u0, left) #Left
    #bc2 = DirichletBC(V, u1, top) #Top
    bc3 = DirichletBC(V, u0, right) #Right
    bcs = [bc1, bc3, bc4]#, bc2]    
    #Variational Problem
    problem = NonlinearVariationalProblem(F, u, bcs, J=J)
    solver = NonlinearVariationalSolver(problem)
    solver.parameters.update(snes_solver_parameters)
    info(solver.parameters, True)
    
    
    solver.solve()
#    plot(u)
   # print(u(0.5,1.0)[1])
   # print(ymax)

    #Get Traction  
    TFS = params['TFS']
    sigmaw = project(sigma(u, mu, lmbda), TFS)
    sig = sigma(u, mu, lmbda)
    n = FacetNormal(mesh)
#    print(sigmaw)
#    T = sigmaw(0.5,1.0)[2]
#    print(T)
#    print(sigmaw(0.5,1.0)[3])
#    print(sigmaw(0.5,1.0)[2])
#    print(sigmaw(0.5,1.0)[1])
#    print(sigmaw(0.5,1.0)[0])

    #Force = assemble(dot(T*ds(1)))
    tau = dot(sig,n)
    fy = assemble(tau[1]*ds)

    results = [sig, fy]

    return results


def initializeI(input):
    global mu
    global lmbda
    global h0
    mesh = domain()
    params = {}
    params['mesh'] = mesh
    E = input[0] #Elastic Modulus
    nu = input[1] #Poisson Ratio
    mu = E/(2.0*(1.0 + nu))  #Lame constants
    lmbda = E*nu/((1.0 + nu)*(1.0 - 2.0*nu))
    rho = 3.98
    h0 = input[2]
    S = input[3]
    Ss = input[4]
    mm = input[5]
    d = input[6]
    a = input[7]
    params['Ss'] = Ss
    params['S'] = S
    params['h0'] = h0
    params['rho'] = rho
    #Create Function Space
    V = VectorFunctionSpace(mesh, "Lagrange", 1)
    params['V'] = V
    W = FunctionSpace(mesh, "Lagrange", 1)
    params['W']= W
    gamma = Function(W)
    gamma = interpolate(Constant(0.0), W)
    params['gamma'] = gamma
    params['TFS'] = TensorFunctionSpace(mesh, "Lagrange", 1)
    slipSystems = [];
    m1 = input[8]
    m2 = input[9]
    mnorm = sqr(m1*m1 + m2*m2)
    m1 = m1/mnorm
    m2 = m2/mnorm
    s1 = -m2
    s2 = m1
    basalSlip1 = slipSystem(mesh, Constant((m1,m2)), Constant((s1,s2)), V, params['h0'],params['Ss'], mm, a, d, gamma,W)
    slipSystems.append(basalSlip1)
    params['slipSystems'] = slipSystems
    return params

def nanoIndentSimulation(inputs):
    params = initializeI(inputs)
    f = open('./sapphireData.dat', 'r')
    g = open('simResults.dat', 'w')
    time = []
    forcem = []
    displacement = []
    forced = []
    lasty = 0;
    tlast = 0;
    error = 0;
    for line in f:
        data = line
        datum = data.split('\t')
        t = float(datum[2])
        F = float(datum[1])
        ymax = float(datum[0])/1000
        if datum[0] >= 0:
            if 'tLast' in locals():
                dt = t - tLast
            else:
                dt = t
            params['ymax'] = ymax
            results =elasticCalc(params)
            fy = results[1]
            sig = results[0]
            slipSystemUpdate(params['slipSystems'], dt, sig)
            time.append(t)
            forcem.append(fy)
            forced.append(F)
            displacement.append(ymax)
            dd = np.abs(ymax- lasty)
            error = error + (F - fy)*(F-fy)*dd
            B = slipBodyForce(params['slipSystems'], 2, params['V'])
            #print(B)
            #print('\n')
            #print(sig)
            #print(project(sig))
            #print(B)
            #plot(B, interactive = True)
            #print(sig)
#            print(B)
#            print(norm(B, norm_type = "l2", mesh = mesh))
#            plot(B, interactive)
#            Bp = project(B, params['V'])
#            Bp = project(sig, params['TFS'])
#            print(Bp(0.5,1.0)[1])
#            stop
            
            
            g.write(str((F-fy)*dd) + ',\t' + str(fy)+ ',\t' +  str(F)+ ',\t' +str(t) +',\t' +str(B) +  '\n')
        lasty = ymax;
        tlast = t;
    return error
        

def main():
    input = [1e9, 0.3, 1e6, 1e6, 1e9, .06, .06, 1, 1, 0]
    x = scipy.optimize.minimize(nanoIndentSimulation, input, method='Nelder-Mead')
    error = nanoIndentSimulation(x)
    print(error)
    print(x)
    #params = initialize()
    #params['ymax'] = 0.1;
    #elasticCalc(params)
    
    
if __name__ == '__main__':
  main()
