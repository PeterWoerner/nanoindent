function [ss] = SS_func(theta,data)

params.a = theta(1);
params.b = theta(2);
params.k = theta(3);
[gamma] = surrogate_model_exp(data.t, data.F, data.u, params);
ss = sum((data.F - params.k*(data.u-gamma)).^2);

end
