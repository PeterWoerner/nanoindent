a = ls;
expression = '(\w)+.fig';
matchStr = regexp(a, expression, 'match');
for i = 1:length(matchStr)
    openfig(matchStr{i});
    set(findall(gcf,'-property','FontSize'),'FontSize',20);
    savefig(matchStr{i}(1:length(matchStr{i})-4);
    print(matchStr{i}(1:length(matchStr{i})-4), '-depsc');
end
