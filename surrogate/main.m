clear all
% close all
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
addpath('./mcmcstat')
%Load data from saved previous run (the lines above can be commented if
%this load command is executed)
load sapphireData.dat

u = sapphireData(:,1);
F = sapphireData(:,2);
t = sapphireData(:,3);

% load results

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%put stress and strain into a data structure for the Bayesian analysis
data.t = transpose(t);
data.F = transpose(F);
data.u = transpose(u);
data.xdata= u;
data.ydata = F;


%define initial guess
a = .011289;
b = 1e-3;
k = 87.586; %(spring constant = elastic modulus in GPa)




%model parameter range
params = {
    {'a', a, 0, inf}
    {'b', b, 0, inf}
    {'k', k, 0, inf}
    };

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Define model for parameter optimization 
% SS function and Parameters
ssfun = @SS_func;%(modelpar,dtb1,dtb2,bstretch,bstress);

model.ssfun=ssfun;

legend('Data','Model','Location','SouthWest')

%***************************************************************
% BAYESIAN ANALYSIS
model.sigma2 = 1e-4; %initial guess on variance
model.S20 = model.sigma2;
model.N0  = 1;
options.updatesigma = 1; %update variance as part of the inference
options.method = 'dram'; %this applies the DRAM algorithm
model.N  = length(data.ydata); %number of data points

   
options.nsimu = 10000; %number of iterations in the Metropolis method
[results, chain, s2chain, sschain]= mcmcrun(model,data,params,options);

options.nsimu = 50000; %number of iterations in the Metropolis method
[results, chain, s2chain, sschain]= mcmcrun(model,data,params,options,results);

chainstats(chain,results)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%plot results
figure(1)
mcmcplot(chain,[],results,'denspanel',2);

f.f2 = figure(2);str.f2 = 'Pairs';
mcmcplot(chain,[],results,'pairs');

figure(3); clf
mcmcplot(chain,[],results.names,'chainpanel')
xlabel('Iterations','Fontsize',24)
ylabel('Parameter value','Fontsize',24)

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%prediction interval analysis

modelfun1 = @(d,th)surrogate_model2(th,d); % NOTE: the order in which d and th appear is important

nsample = 500;
out = mcmcpred(results,chain,s2chain,data.xdata,modelfun1,nsample);
[ms, msi] = max(data.xdata(:));
out.predlims
% separate intervals for plotting
out_1.xdata = data.xdata(1:msi);
out_1.predlims{1}{1} = out.predlims{1}{1}(:,1:msi);
out_1.obslims{1}{1} = out.obslims{1}{1}(:,1:msi);
out_2.xdata = data.xdata(msi:end);
out_2.predlims{1}{1} = out.predlims{1}{1}(:,msi:end);
out_2.obslims{1}{1} = out.obslims{1}{1}(:,msi:end);
mcmcpredplot_overlap(out_1, out_2);
%modelout = mcmcpredplot(out); %,data
hold on
plot(data.xdata,data.ydata,'b.--','linewidth',1)
hold off
xlabel('u(nm)','Fontsize',30);
ylabel('F(nN)','Interpreter','latex','Fontsize',30);
legend('95% Prediction Interval', '95% Prediction Interval', '95% Confidence Interval','95% Confidence Interval','Model Density Fit','Model Density Fit','Experimental Values','Location','Best')



