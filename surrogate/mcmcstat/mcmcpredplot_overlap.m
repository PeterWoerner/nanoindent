function [h] = mcmcpredplot_overlap(out_1, out_2, h)

if nargin < 3 || isempty(h)
    h = figure;
end

% unpack input arguments
time1 = out_1.xdata;
time2 = out_2.xdata;
% Generate predplot
np = size(out_1.predlims{1}{1},1);
nn = (np+1)/2; % median

dimc = [0.4 0.9 0.7]; % dimmest (lightest) color
i = 1; j = 1;
if ~isempty(out_1.obslims)
    hold on
    fillyy(time1,out_1.obslims{i}{j}(1,:),out_1.obslims{i}{j}(3,:),dimc);
    fillyy(time2,out_2.obslims{i}{j}(1,:),out_2.obslims{i}{j}(3,:),dimc);
    hold on
    dimc = [0.8,0.4,0.4];
end
fillyy(time1,out_1.predlims{i}{j}(1,:),out_1.predlims{i}{j}(2*nn-1,:),dimc);
fillyy(time2,out_2.predlims{i}{j}(1,:),out_2.predlims{i}{j}(2*nn-1,:),dimc);
hold on
h_med = plot(time1,out_1.predlims{i}{j}(nn,:),'-k','LineWidth',2);
plot(time2,out_2.predlims{i}{j}(nn,:),'-k','LineWidth',2);
hold off
end