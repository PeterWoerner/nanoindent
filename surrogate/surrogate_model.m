function [gamma] = surrogate_model(t, F, u, params )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
k = params.k;
l = length(t);
gamma(1) = 0;
y(1) = u(1) - F(1)/2;
gammaDot = model(F, params.a);
for i = 2:l
    dt = t(i) - t(i-1);
    y(i) = u(i) - F(i)/k;
    gamma(i) = (gammaDot(i) + gammaDot(i-1))/2*dt + gamma(i-1);
end
    
%ssError = sum((gamma - y).^2);


end


function [gammaDot] = model(F, a)
gammaDot = a*F;
end