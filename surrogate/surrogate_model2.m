function [r] = surrogate_model(theta, u )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
load sapphireData.dat
%u = sapphireData(:,1);
F = sapphireData(:,2);
t = sapphireData(:,3);
k = theta(3);
l = length(t);
gamma(1) = 0;
y(1) = u(1) - F(1)/2;
gammaDot = model(F, theta(1), theta(2));
for i = 2:l
    dt = t(i) - t(i-1);
    y(i) = u(i) - F(i)/k;
    gamma(i) = (gammaDot(i) + gammaDot(i-1))/2*dt + gamma(i-1);
end
    
ssError = sum((gamma - y).^2);
r = k*(u-transpose(gamma));

end


function [gammaDot] = model(F, a, b)
gammaDot = a*exp(b*F);
end