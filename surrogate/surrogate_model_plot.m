function [gamma] = surrogate_model(theta )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
load sapphireData.dat
u = sapphireData(:,1);
F = sapphireData(:,2);
t = sapphireData(:,3);
k = theta(2);
l = length(t);
gamma(1) = 0;
y(1) = u(1) - F(1)/2;
gammaDot = model(F, theta(1));
for i = 2:l
    dt = t(i) - t(i-1);
    y(i) = u(i) - F(i)/k;
    gamma(i) = (gammaDot(i) + gammaDot(i-1))/2*dt + gamma(i-1);
end
    
ssError = sum((gamma - y).^2)
plot(u,F)
hold on
size(u)
size(gamma)
plot(u, k*(u-transpose(gamma)))



end


function [gammaDot] = model(F, a)
gammaDot = a*F;
end