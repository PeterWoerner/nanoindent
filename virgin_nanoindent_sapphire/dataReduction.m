load virginsapphire_exp1.dat

displacement = virginsapphire_exp1(:,1);
force = virginsapphire_exp1(:,2);
t = virginsapphire_exp1(:,3);

xx = linspace(1.35, max(t), 500);

loadS = spline(t, force, xx);
dispS = spline(t, displacement, xx);
fileID = fopen('sapphireData.dat', 'w')
for i = 1: length(xx)
    fprintf(fileID, '%f \t %f \t %f \n', dispS(i), loadS(i), t(i));
end

figure
plot(dispS,loadS, 'r--o')
hold on
plot(displacement, force)
    