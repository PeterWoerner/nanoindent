load virginsapphire_exp1.dat

x1 = virginsapphire_exp1(:,1);
x2 = virginsapphire_exp1(:,2);
x3 = virginsapphire_exp1(:,3);

load virginsapphire_exp2.dat

y1 = virginsapphire_exp2(:,1);
y2 = virginsapphire_exp2(:,2);
y3 = virginsapphire_exp2(:,3);


load virginsapphire_exp3.dat

z1 = virginsapphire_exp3(:,1);
z2 = virginsapphire_exp3(:,2);
z3 = virginsapphire_exp3(:,3);


load virginsapphire_exp4.dat

w1 = virginsapphire_exp4(:,1);
w2 = virginsapphire_exp4(:,2);
w3 = virginsapphire_exp4(:,3);


figure(1)
hold on
plot(x1(80:end),x2(80:end))
plot(y1(80:end),y2(80:end))
plot(z1(80:end),z2(80:end))
plot(w1(80:end),w2(80:end))

figure(2)
hold on
plot(x3(80:end),x1(80:end))
plot(y3(80:end),y1(80:end))
plot(z3(80:end),z1(80:end))
plot(w3(80:end),w1(80:end))