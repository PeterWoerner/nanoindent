# -*- coding: utf-8 -*-
"""
Created on Fri Feb 17 11:01:09 2017
Weak Form, combined theta, integration
@author: peter
"""

## Import libraries
from __future__ import print_function
from dolfin import *
import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize

class Left(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[0], 0.0)

class Right(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[0], 1.0)

class Bottom(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[1], 0.0)

class Top(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[1], 1.0)

class elasticplasticEquation(NonlinearProblem):
    def __init__(self, a, L, bcs):
       NonlinearProblem.__init__(self)
       self.L = L
       self.a = a
       self.bcs = bcs
    def F(self, b, x):
        assemble(self.L, tensor =b)
        for bc in self.bcs:
           bc.apply(b,x)
    def J(self, A, x):
        assemble(self.a, tensor = A)
        for bc in self.bcs:
           bc.apply(A)


# init stores the linear(L) and bilinear (a) forms
# Fand J are virtual member fuctions of the py:class: NonlinearProblem.
# F computes the residual vector b, and J compute the Jacobian matrix A


#FEA is the engine of the finite element analysis.  It creates the
def FEA(input):
    a1, b, lmbda, mu,  = input[0], input[1], input[2], input[3]
    print([a1, b, lmbda, mu])
    #Will eventually go into an initialization file for defining parameters and params will be passed into this function
    s = Expression(('1', '0'))
    m = Expression(('0', '1'))
    ms = as_tensor(m[i]*s[j],(i,j))
    sm = as_tensor(m[j]*s[i],(i,j))
    slip = SlipSystem(m, s, lmbda, mu, a1, b)
    #Define mesh and space
    mesh = UnitSquareMesh(30,30)
    SF = FunctionSpace(mesh, "Lagrange", 1)
    VF = VectorFunctionSpace(mesh, "Lagrange", 1)
    M = MixedFunctionSpace([VF, SF])
    #we define our vector as u_x u_y, gamma

    #trial functions
    dv = TrialFunction(M)
    uu, gam = TestFunctions(M)

    #Define Functions
    v = Function(M) #current solution
    v0 = Function(M) #solution from previous time step

    du, dgamma = split(dv)
    u, gamma = split(v)
    u0, gamma0 = split(v0)


    #define initial conditions

    v_init = Expression(("0", "0", "0"))
    v.interpolate(v_init)
    v0.interpolate(v_init)
    print("Test Point 1")


    #Define midpoints for convenience
    theta = 0.5
    print(gamma0)
    print(gamma)
    gamma_mid = (1.0 - theta)*gamma0 + theta*gamma
    u_mid = (1.0 - theta)*u0 + theta*u

    #Define indentation Expression
    #ymax = params['ymax']
    ymax = 0.1;
    u1 = Expression((("0.0","std::min(sin(pi/3)*std::abs(x[0]-0.5)-y,0.0)")), y=ymax)

#Define Boundary Conditions
    left = Left()
    top = Top()

    right = Right()
    bottom = Bottom()
    u0 = Constant((0.0,0.0))
    bc4 = DirichletBC(VF, u0, bottom) #Bottom
    bc1 = DirichletBC(VF, u0, left) #Left
    #bc2 = DirichletBC(V, u1, top) #Top
    bc3 = DirichletBC(VF, u0, right) #Right
    bcs = [bc1, bc3, bc4]#, bc2]
    exterior_facet_domains = FacetFunction("uint", mesh)
    exterior_facet_domains.set_all(0)
    top.mark(exterior_facet_domains,1)
    dss = ds[exterior_facet_domains]

    #Weak Forms
    #Lg = gamma*gam/dt*dx - gamma0*gam/dt*dx + a0*inner(grad(gamma_mid),grad(gam))*dx + gam*slip.gammadot(u_mid,gamma_mid)*dx
    #Lu = -(lmbda+mu)*div(u_mid)*div(uu)*dx-mu*inner(grad(u_mid),grad(uu))*dx \
    #     - mu*dot(uu, dot(grad(gamma_mid), ms - sm))*dx
    #Lb = -1e12*(np.abs(u1[1]-u[1]) - (u1[1]-u[1]))*uu[1]*dss(1) #Spring like penalty on the boundary
         #+ rho*B to include body forces later and penalty expression
    #L = Lg + Lu + Lb #Summing up the weak forms creates the total weak form

    #L = div(u)*div(uu)*dx + inner(grad(gamma),grad(gam))*dx
    #Compute directional derivative about u in the direction of L
    #a = derivative(L,v,dv)
    #print('Derviative Calc')

    #problem = elasticplasticEquation(a, L, bcs)
    solver = NewtonSolver()
    solver.parameters['linear_solver'] = 'lu'
    solver.parameters['convergence_criterion'] = "incremental"
    solver.parameters['relative_tolerance'] = 1e-8
    solver.parameters['absolute_tolerance'] = 1e-10
    solver.parameters['maximum_iterations'] = 20
    solver.parameters["error_on_nonconvergence"] = False
    file = File("output.pvd", "compressed")
    f = open('./sapphireData50.dat', 'r')
    lasty = 0.0
    tlast = 0.0
    time = []
    forceModel = []
    forceData = []
    displaceModel = []
    displaceData= []
    error = 0.0
    for line in f:
        data = line
        datum = data.split('\t')
        t = float(datum[2])
        if 'tLast' in locals():
           dt = t - tLast
        else:
           dt = t
        print(t)
        if dt ==0:
          tlast = 0;
        else:
          F = float(datum[1])/1000.0 #force is measure in microNetwons
          ymax = float(datum[0])/1000.0 #the scale of the space is in micrometers
          u1 = Expression((("0.0","std::min(sin(pi/3)*std::abs(x[0]-0.5)-y,0.0)")), y=ymax)#
          Lg = gamma*gam*dx - gamma0*gam*dx + dt*gam*slip.gammadot(u_mid,gamma_mid)*dx# + dt*a0*inner(grad(gamma_mid),grad(gam))*dx
          Lu = -(lmbda+mu)*div(u_mid)*div(uu)*dx-mu*inner(grad(u_mid),grad(uu))*dx \
               - mu*dot(uu, dot(grad(gamma_mid), ms - sm))*dx
          Lb = -1e15*(np.abs(u1[1]-u[1]) - (u1[1]-u[1]))*uu[1]*dss(1)
          L = Lg + Lu + Lb
          a = derivative(L,v,dv)
          problem = elasticplasticEquation(a, L, bcs)
          solver.solve(problem, v.vector())
          v0.vector()[:] = v.vector()
          dM = v(0.5,1)[1]
          epsilon = slip.strain(u, gamma)
          sigma = slip.stress(epsilon)
          n = FacetNormal(mesh)
          tau = dot(sigma, n)
          fy = -assemble(tau[1]*dss(1))

          forceModel.append(fy)
          forceData.append(F)
          displaceModel.append(dM)
          displaceData.append(ymax)
          dd = np.abs(ymax- lasty)
          error = error + (F-fy)*(F-fy)*dd
          file << v.split()[0],t
#    plot(u, mode = "displacement", title = "u")
#    plot(gamma, title = "Gamma_Unloaded")
    print(displaceData)
    print(forceData)
    print(forceModel)
    print(displaceModel)
    print(forceData)
    plot(mesh)
    interactive()
    return error

class SlipSystem():
  def __init__(self, m, s, lmbda, mu, a, b):  #start with linear system f= a*tau
     self.m = m
     self.s = s
     self.a = a
     self.b = b
     self.lmbda = lmbda
     self.mu = mu
  def gammadot(self, u,gamma):
    epsilon = self.strain(u,gamma)
    sigma = self.stress(epsilon)
    tau = dot(dot(self.s, sigma), self.m)
    gd = self.f(tau, gamma)
    return gd
  def stress(self, epsilon):
    sigma = 2*self.mu*epsilon + self.lmbda*tr(epsilon)*Identity(2)
    return sigma
  def strain(self, u,gamma):
    ms = as_tensor(self.m[j]*self.s[i],(i,j))
    epsilon = grad(u) - gamma*ms
    return epsilon
  def f(self, tau, gamma):
    return self.a*exp(-abs(tau)*self.b)*sign(tau)

def Indent():
    params = {}
    params['a1'] = 1e-3;
    sqrt2 = sqrt(2)
    params['s'] = Expression(('1/sqrt2', '1/sqrt2'), sqrt2 = sqrt2)
    params['m'] = Expression(('1/sqrt2', '-1/sqrt2'), sqrt2 = sqrt2)
    params['s'] = Expression(('1', '0'), sqrt2 = sqrt2)
    params['m'] = Expression(('0', '1'), sqrt2 = sqrt2)
    params['lmbda'] = 145;
    params['mu'] = 145;
    #Define model parameters (this will be passed to a separate function in the future)
    theta = 0.5
    a1 = params['a1']
    a0 = 0
    b = 1
    s = params['s']
    m = params['m']
    mu = params['mu']
    lmbda = params['lmbda']
    error = FEA([a1, b, lmbda, mu])


def main():
    #Indent()
    input = [1e0, 1, 145e6, 145e6]
    #Indent()
    FEA(input)
    #print('Begin Optimization')
    #x = scipy.optimize.minimize(FEA, input, method='Nelder-Mead')
    #print('End Optimization')
    #print(x)
    #FEA(x)

if __name__ == '__main__':
    main()
